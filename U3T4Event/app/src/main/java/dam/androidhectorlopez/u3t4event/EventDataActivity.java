package dam.androidhectorlopez.u3t4event;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private String priority = "Normal";
    final Calendar c = Calendar.getInstance();
    private TextView tvEventName, tvPlace;
    private RadioGroup rgPriority;

    private Button btAccept;
    private Button btCancel;
    private Button btDate;
    private Button btTime;
    private String[] month;
    private EditText etPlace;
    private int mYear, mMonth, mDay, mHour, mMinute;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle intputData = getIntent().getExtras();

        // TODO: 01/12/2019 Aci carreguem l'array de mesos que tenim en el fitxer strings.xml
        month = getResources().getStringArray(R.array.arrMonths);

        tvEventName.setText(intputData.getString("EventName"));
        String cadena = intputData.getString("Contingut");
        if (!cadena.isEmpty()) {
            setUpdate(cadena);
        }


    }

    // TODO: 02/12/2019 En aquest metode fiquem les dades de l'event anterior quan afegim un nou event
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setUpdate(String cadena) {
        String[] parts = cadena.split("\n");

        String hora = "";
        String mes = "";
        String prioritat = "";
        String lloc = "";

        for (int i = 0; i < parts.length; i++) {
            if (parts[i].contains("Place:")) {
                lloc = parts[i].substring(7);
                etPlace.setText(lloc);
            } else if (parts[i].contains("Month:")) {
                mes = parts[i].substring(7);

            } else if (parts[i].contains("Day:")) {
                String d = parts[i].substring(5);
                mDay = Integer.parseInt(d);

            } else if (parts[i].contains("Year:")) {
                String any = parts[i].substring(6);
                mYear = Integer.parseInt(any);

            } else if (parts[i].contains("Time:")) {
                hora = parts[i].substring(6);
                String[] time = hora.split(":");
                mHour = (Integer.parseInt(time[0]));
                mMinute = (Integer.parseInt(time[1]));


            } else if (parts[i].contains("Priority:")) {
                prioritat = parts[i].substring(10);

                if (prioritat.equals("Low")) {
                    rgPriority.check(R.id.rbLow);
                } else if (prioritat.equals("High")) {
                    rgPriority.check(R.id.rbHigh);
                } else {
                    rgPriority.check(R.id.rbNormal);
                }
            }

        }
        // TODO: 03/12/2019 Aci agafem el numero de mes que tenim
        boolean pasa = true;
        for (int i = 0; i < month.length && pasa; i++) {
            if (month[i].equals(mes)) {
                mMonth = i;
                pasa = false;
            }
        }

    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        tvPlace = findViewById(R.id.tvPlace);
        etPlace = findViewById(R.id.etPlace);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);


        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);
        btDate = findViewById(R.id.btDate);
        btTime = findViewById(R.id.btTime);

        btDate.setOnClickListener(this);
        btTime.setOnClickListener(this);
        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();
        switch (v.getId()) {

            case R.id.btAccept:
                eventData.putString("EventData", "Place: " + etPlace.getText() + "\n" +
                        "Priority: " + priority + "\n" +
                        "Month: " + month[mMonth] + "\n" +
                        "Day: " + mDay + "\n" +
                        "Year: " + mYear + "\n" +
                        "Time: " + mHour + ":" + mMinute);
                activityResult.putExtras(eventData);
                setResult(RESULT_OK, activityResult);
                finish();
                break;

            // TODO: 01/12/2019 Aci en cas de que es presione la tecla cancelar s'envia una cadena buida
            case R.id.btCancel:
                eventData.putString("EventData", "");
                activityResult.putExtras(eventData);
                setResult(RESULT_CANCELED, activityResult);
                finish();
                break;

            case R.id.btDate:
                showDatePickerDialog();
                break;

            case R.id.btTime:
                showTimePickerDialog();
                break;
        }


    }

    // TODO: 03/12/2019 Aci es per a assignar la prioritat segons l'element que hagem tirat
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rbLow:
                priority = "Low";
                break;

            case R.id.rbNormal:
                priority = "Normal";
                break;

            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }

    // TODO: 02/12/2019 Aci sabem quina es la rotacio de la pantalla 
    public String getRotation(Context context) {
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                return "vertical";
            case Surface.ROTATION_90:
            default:
                return "horizontal";
        }
    }

    // TODO: 03/12/2019 Aci mostrem el dialeg d'agafar la data
    private void showDatePickerDialog() {
        // Get Current Date
        final int mes;
        final int dia;
        final int anio;

        if (mYear == 0 && mMonth == 0 && mDay == 0) {
            mes = c.get(Calendar.MONTH);
            dia = c.get(Calendar.DAY_OF_MONTH);
            anio = c.get(Calendar.YEAR);
        } else {
            anio = mYear;
            mes = mMonth;
            dia = mDay;
        }

        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int any, int mes, int diaMes) {
                mMonth = mes;
                mYear = any;
                mDay = diaMes;
            }
        }, anio, mes, dia);

        recogerFecha.show();

    }

    // TODO: 03/12/2019 Aci mostrem el dialeg d'agafar l'hora
    private void showTimePickerDialog() {
        final int hour;
        final int minute;

        if (mHour == 0 && mMinute == 0) {
            hour = c.get(Calendar.HOUR_OF_DAY);
            minute = c.get(Calendar.MINUTE);
        } else {
            hour = mHour;
            minute = mMinute;
        }

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                mHour = selectedHour;
                mMinute = selectedMinute;
            }
        }, hour, minute, true);//Yes 24 hour time

        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

}


