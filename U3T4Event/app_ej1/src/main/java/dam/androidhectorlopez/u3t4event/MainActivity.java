package dam.androidhectorlopez.u3t4event;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private final int REQUEST = 0;

    private EditText etEventName;
    private TextView tvCurrentData;
    private String contingut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        etEventName = findViewById(R.id.etEventName);
        tvCurrentData = findViewById(R.id.tvCurrentData);
        tvCurrentData.setText("");
    }

    public void editEventData(View view) {
        Intent intent = new Intent(this, EventDataActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("EventName", etEventName.getText().toString());

        bundle.putString("Contingut", tvCurrentData.getText().toString());

        intent.putExtras(bundle);

        startActivityForResult(intent, REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // TODO: 01/12/2019 Aci si polsem la tecla ACCEPTAR ens fica el contingut que li hem passat a traves de l'intent 
        if (requestCode == REQUEST && resultCode == RESULT_OK) {
//            tvCurrentData.setText(data.getStringExtra("EventData"));
            contingut = data.getStringExtra("EventData");
            tvCurrentData.setText(contingut);

            // TODO: 01/12/2019 aci en cas de que es polse la tecla cancelar es fica en pantalla el contingut anterior
        } else if (requestCode == REQUEST && resultCode == RESULT_CANCELED) {
            tvCurrentData.setText(contingut);

        }

    }

    // TODO: 02/12/2019 Aci guardem el contingut del textView 
    @Override
    protected void onSaveInstanceState(Bundle guardar) {
        super.onSaveInstanceState(guardar);
        guardar.putString("guarda", tvCurrentData.getText().toString());

    }

    // TODO: 02/12/2019 Aci recuperem el contingut del textView
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle recuperar) {
        super.onRestoreInstanceState(recuperar);
        tvCurrentData.setText(recuperar.getString("guarda"));
    }
}
