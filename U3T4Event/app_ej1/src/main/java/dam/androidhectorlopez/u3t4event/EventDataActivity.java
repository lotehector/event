package dam.androidhectorlopez.u3t4event;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private String priority = "Normal";

    private TextView tvEventName, tvPlace;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;
    private String[] month;
    private EditText etPlace;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle intputData = getIntent().getExtras();

        // TODO: 01/12/2019 Aci carreguem l'array de mesos que tenim en el fitxer strings.xml
        month = getResources().getStringArray(R.array.arrMonths);

        tvEventName.setText(intputData.getString("EventName"));
//        System.out.println(intputData.getString("Contingut"));
        String cadena = intputData.getString("Contingut");
        if (!cadena.isEmpty()) {
            setUpdate(cadena);
        }
    }

    // TODO: 02/12/2019 En aquest metode fiquem les dades de l'event anterior quan afegim un nou event 
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setUpdate(String cadena) {
        String[] parts = cadena.split("\n");
        int numDay = 0;
        int year = 0;
        int meso = 0;
        String hora = "";
        String mes = "";
        String prioritat = "";
        String lloc = "";
        for (int i = 0; i < parts.length; i++) {
            System.out.println(parts[i] + " se");
            if (parts[i].contains("Place:")) {
                lloc = parts[i].substring(7);
                etPlace.setText(lloc);
            } else if (parts[i].contains("Month:")) {
                mes = parts[i].substring(7);

                System.out.println("Meeees " + mes);
            } else if (parts[i].contains("Day:")) {
                String d = parts[i].substring(5);
                numDay = Integer.parseInt(d);

            } else if (parts[i].contains("Year:")) {
                String any = parts[i].substring(6);
                year = Integer.parseInt(any);

            } else if (parts[i].contains("Time:")) {
                hora = parts[i].substring(6);
                String[] time = hora.split(":");
                tpTime.setHour(Integer.parseInt(time[0]));
                tpTime.setMinute(Integer.parseInt(time[1]));


            } else if (parts[i].contains("Priority:")) {
                prioritat = parts[i].substring(10);

                if (prioritat.equals("Low")) {
                    rgPriority.check(R.id.rbLow);
                } else if (prioritat.equals("High")) {
                    rgPriority.check(R.id.rbHigh);
                } else {
                    rgPriority.check(R.id.rbNormal);
                }
            }

        }
        boolean pasa = true;
        for (int i = 0; i < month.length && pasa; i++) {
            if (month[i].equals(mes)) {
                meso = i;
                pasa = false;
            }
        }

        dpDate.updateDate(year, meso, numDay);
    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        tvPlace = findViewById(R.id.tvPlace);
        etPlace = findViewById(R.id.etPlace);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);

        dpDate = findViewById(R.id.dpDate);
        tpTime = findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);


        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();
        switch (v.getId()) {

            case R.id.btAccept:
                eventData.putString("EventData", "Place: " + etPlace.getText() + "\n" +
                        "Priority: " + priority + "\n" +
                        "Month: " + month[dpDate.getMonth()] + "\n" +
                        "Day: " + dpDate.getDayOfMonth() + "\n" +
                        "Year: " + dpDate.getYear() + "\n" +
                        "Time: " + tpTime.getHour() + ":" + tpTime.getMinute());
                activityResult.putExtras(eventData);
                setResult(RESULT_OK, activityResult);
                break;

            // TODO: 01/12/2019 Aci en cas de que es presione la tecla cancelar s'envia una cadena buida
            case R.id.btCancel:
                eventData.putString("EventData", "");
                activityResult.putExtras(eventData);
                setResult(RESULT_CANCELED, activityResult);
                break;
        }


        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rbLow:
                priority = "Low";
                break;

            case R.id.rbNormal:
                priority = "Normal";
                break;

            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }
}
